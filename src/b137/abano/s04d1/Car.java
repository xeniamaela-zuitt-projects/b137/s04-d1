package b137.abano.s04d1;

public class Car {
    //Properties
    private String name;
    private String brand;
    private int yearOfMake;

    //Constructor
        //1. Empty Constructor
        public Car() {}

        //2. Parameterized constructor
        public Car(String name, String brand, int yearOfMake){
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
        }

    //Getters - retrieving the value
        public String getName() {
            return name;
        };

        public String getBrand() {
            return brand;
        };

        public int yearOfMake() {
            return yearOfMake;
        };

    //Setters - set or update
        public void setName(String newName) {
            this.name = newName;
        };

        public void setBrand(String newBrand) {
            this.brand = newBrand;
        };

        public void setYearOfMake(int newYearOfMake) {
            this.yearOfMake = newYearOfMake;
        };
    //Methods
        public void drive() {
            System.out.println("I am driving " + this.name + " " + this.brand + " " + this.yearOfMake);
        }
}
