package b137.abano.s04d1;

public class Main {
    public static void main(String[] args){
        Car firstCar = new Car();

        //invoke setName
        firstCar.setName("Ertiga");
        firstCar.setBrand("Suzuki");
        firstCar.setYearOfMake(2000);


        //invoke method
        firstCar.drive();

        //to see the value
/*        System.out.println(firstCar.getName());
        System.out.println(firstCar.getBrand());
        System.out.println(firstCar.getYearOfMake());*/

        Car secondCar = new Car("Avanza", "Toyota", 2005);

        secondCar.drive();

    }
}
